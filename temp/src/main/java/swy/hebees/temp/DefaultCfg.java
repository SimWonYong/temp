package swy.hebees.temp;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import swy.hebees.temp.board.repository.BoardRepository;

import javax.persistence.EntityManager;

@Configuration
@RequiredArgsConstructor
public class DefaultCfg implements WebMvcConfigurer {

    private EntityManager em;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/");
    }

    @Bean
    public BoardRepository boardRepository(){
        return new BoardRepository(em);
    }


}
