package swy.hebees.temp.board.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter @Setter
@Entity
public class Post {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long num;

    private String title;
    private String author;
    private String password;
    private String content;

    @Override
    public String toString() {
        return "Post{" +
//                "num=" + num +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", password='" + password + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
