package swy.hebees.temp.board.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import swy.hebees.temp.board.dto.Post;
import swy.hebees.temp.board.service.BoardService;

@Controller
@RequiredArgsConstructor
public class BoardController {

    private final BoardService boardService;

    //인덱스 페이지
    @GetMapping("/home")
    public String home(Model model){
        model.addAttribute("data", "value");
        return "index.html";
    }

    //게시판 목록
    @GetMapping("/boardList")
    public String boardList(){
        return "boardList.html";
    }

    //글 작성 페이지 조회
    @GetMapping("/boardForm")
    public String boardForm(){
        return "boardForm.html";
    }

    //글 작성 완료
    @PostMapping("/boardFormWrite")
    public String boardFormWrite(Post post){
//        System.out.println(post.getAuthor());
//        System.out.println(post.getTitle());
//        System.out.println(post.getPassword());
//        System.out.println(post.getContent());
        boardService.save(post);
        return "redirect:/boardList";
    }

}
