package swy.hebees.temp.board.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import swy.hebees.temp.board.dto.Post;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RequiredArgsConstructor
public class BoardRepository {

//    private final DataSource dataSource;
    private final EntityManager em;

    public void save(Post post) {

        System.out.println("============================");
        System.out.println("============================");
        System.out.println("============================");
        System.out.println("============================");
        System.out.println("============================");
        System.out.println(post);
        em.persist(post);

//        Connection conn;
//        String sql = "select 1 from dual";
//        PreparedStatement pstmt;
//        ResultSet rs;
//
//        try {
//
//            conn = dataSource.getConnection();
//            pstmt = conn.prepareStatement(sql);
//            rs = pstmt.executeQuery();
//
//            if(rs.next()){
//                String str = rs.getString(1 );
//                System.out.println(str);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

    }

    public Post findPost(Long num){
        return em.createQuery("select b from board b where b.num = :num", Post.class)
                .setParameter("num", num)
                .getSingleResult();
    }
}
