package swy.hebees.temp.board.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import swy.hebees.temp.board.dto.Post;
import swy.hebees.temp.board.repository.BoardRepository;

@Service
@RequiredArgsConstructor
public class BoardService {

    private final BoardRepository boardRepository;

    public void save(Post post) {
        boardRepository.save(post);
    }
}
